module Jekyll_RSS_merger

  class FeedMerger
    include Jekyll_Translator

    def self.merge(site)
      feeds = site.data['rss_feeds'] # from Jekyll_Get_Remote_Content
      if !feeds
        return
      end
      config = site.config['merge_rss']

      config.each do |toMerge|
        allFeeds = toMerge['feeds'].map { |feed|
          {
            'name' => feed['name'],
            'items' => feeds[feed['name']]['items'],
            'language' => feed['language']
          }
        }
        
        flattenedItems = []
        allFeeds.each_with_index do |feedData, index|
          flattenedItems = flattenedItems + feedData['items'].map { |item| {
            'content' => item,
            'feedIndex' => index
          }}
        end

        sortedItems = flattenedItems.sort_by { |item| item['content']['published'] }.reverse

        # Translate
        ramaingingTranslateSlots = toMerge['limit'] ? toMerge['limit'] : sortedItems.length
        sortedItems.each do |item|
          content = item['content']

          sourceLang = allFeeds[item['feedIndex']]['language']
          if sourceLang && (ramaingingTranslateSlots > 0)
            translatedTitle = Jekyll_Translator::StringTranslator.translate(content['title'], sourceLang)
            if translatedTitle != content['title']
              content['translatedTitle'] = translatedTitle
            end
            ramaingingTranslateSlots = ramaingingTranslateSlots - 1
          end
        end

        feeds[toMerge['key']] = {
          'name' => toMerge['key'],
          'title' => toMerge['title'],
          'items' => sortedItems.map { |item| item['content'] }
        }
      end
    end
  end

end

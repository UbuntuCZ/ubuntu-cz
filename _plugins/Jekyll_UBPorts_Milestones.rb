require 'open-uri'
require 'json'

module UBportsMilestones
  include Jekyll_Translator

  class Fetcher
    def self.fetchFor(site)
      site.data['UBportsMilestones'] = getMilestones()
    end

    def self.getMilestones()
      if !ENV['gitlabApiKey']
        return nil # Ignore if no api key is present
      end

      milestones = nil
      begin
        URI('https://gitlab.com/api/v4/groups/53986711/milestones').open('r', 'PRIVATE-TOKEN' => ENV['gitlabApiKey']) do |milestonesContent|
          if milestonesContent.status.include? "200"
            milestones = JSON.parse(milestonesContent.read)
          end
        end
        rescue OpenURI::HTTPError
      end

      if milestones
        # Pick active and other (inactive) milestones
        sortedMilestones = milestones.sort_by { |milestone| milestone['due_date'].to_date }
        activeMilestones = sortedMilestones.select { |milestone| milestone['state'] == 'active' }
        otherMilestones = sortedMilestones.select { |milestone| milestone['state'] != 'active' }
        # Take one inactive (if present), add all active and then take first 4, so [ inactive, active, active, active ], and sort by oldest to newest
        mergedMilestones = otherMilestones.slice(0, 1).concat(activeMilestones).first(4).sort_by { |milestone| milestone['due_date'].to_date }.reverse

        return mergedMilestones.map { |milestone| {
          'id' => milestone['id'],
          'title' => milestone['title'] ? Jekyll_Translator::StringTranslator.translate(milestone['title'], 'EN') : '',
          'description' => milestone['description'] ? Jekyll_Translator::StringTranslator.translate(milestone['description'], 'EN') : '',
          'state' => milestone['state'],
          'released' => milestone['due_date'].to_date,
          'created' => milestone['created_at'].to_date,
          'lastUpdated' => milestone['updated_at'].to_date,
          'start' => milestone['start_date'].to_date,
          'url' => milestone['web_url']
        }}
      end
      return nil
    end
  end

  Jekyll::Hooks.register :site, :post_read do |site|
    Fetcher.fetchFor(site)
  end
end

require 'json'
require 'open-uri'
require 'open_uri_redirections'
require 'feedparser'

module Jekyll_Get_Remote_Content

  class FeedFetcher
    def self.fetchFor(site)
      config = site.config['remote_content']
      if !config
        return
      end
      if !config.kind_of?(Array)
        config = [config]
      end
      site.data['rss_feeds'] = Hash.new
      config.each do |remote|
        URI.open(remote['url'], 'r', :allow_redirections => :all) do |remote_content|
          rss_content = remote_content.read.encode('UTF-8', invalid: :replace, replace: '')
          site.data['rss_feeds'][remote['name']] = JSON.parse(FeedParser::Parser.parse(rss_content).to_json)
          site.data['rss_feeds'][remote['name']]['url'] = remote['url']

          site.data['rss_feeds'][remote['name']]['items'].each do |item|
            item['title'] = CGI.unescapeHTML(item['title'].gsub(/<[^>]*>/, '').gsub(/<\/[^>]*>/, '')) # ~strip_tags + unescape
          end
        end
      end
    end
  end

end

# Ubuntu.cz

Repositář pro web Ubuntu.cz. Autorem jeho vzhledu, obsahu a původní verze (jako *iUbuntu.cz*) v PHP je [Martin Kozub](https://gitlab.com/zubozrout). O přepis do statického generátoru [Jekyll](https://jekyllrb.com/) se postaral [Michal Stanke](https://gitlab.com/MikkCZ/).

## Úpravy obsahu

### Šablona
Soubory tvořící základní HTML šablonu se nachází v adresářích `_includes` a `_layouts`. Jako základní styl se používá [Vanilla Framework](https://vanillaframework.io/) od Canonicalu.

### Obsah
Obsah všech stránek je v adresáři `_pages` a je psaný v HTML. Použité obrázky jsou nahrané v adresáři `assets/img`.

### Příprava
Abyste byli schopni spustit níže uvedené příkazy, je nutné mít nainstalované [Ruby 3.2](https://www.ruby-lang.org/en/documentation/installation/) a GNU Make.

Před prvním sestavením (nebo po změně souboru `Gemfile`) je potřeba stáhnout potřebné závislosti.
```
$ make prepare
```

### Náhled
Při úpravách vzhledu i obsahu je dobré rovnou se podívat na výsledek. Níže uvedený příkaz sestaví obsah repositáře a zpřístupní ho na lokální adrese http://localhost:4000/.
```
$ make run
```
Příkaz stačí spustit jednou v samostatném terminálu a nechat běžet. Pokud pak ve zdrojových souborech provedete nějakou změnu, Jekyll sestaví stránky znovu. Pro zobrazení efektu změn stačí obnovit načtenou stránku v prohlížeči (*F5*).

## Sestavení statické verze
Pro sestavení webu slouží tento příkaz.
```
$ make build
```
Statická verze stránek je vygenerovaná do adresáře `_site`. Pro nasazení stačí jeho obsah nahrát na server třeba přes FTP.
